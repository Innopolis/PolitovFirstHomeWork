package tests;

import innopolis.politov.dataprocessing.ResourcesProcessor;
import innopolis.politov.parser.EParser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;
//import org.junit.jupiter.api.function.Executable;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by General on 2/14/2017.
 */
class ResourcesProcessorTest {
    ResourcesProcessor resourcesProcessor;
    @BeforeEach
    void setUp() {
        resourcesProcessor = new ResourcesProcessor(null);
    }

    @AfterEach
    void tearDown() {
        resourcesProcessor = null;
    }


    void simpleTestCalculateSumInResources(BigInteger expectedSum, String[]
            resources, String message){
        //act
        BigInteger sum = resourcesProcessor.calculateSumInResources(resources);
        //assert
        assertEquals(expectedSum, sum, message);
    }

    @Test
    void calculateSumInResourcesSingleThread() {
        //arrange
        String[] resouces = new String[]{"src\\test\\java\\tests\\resources\\positiveNumberSum100.txt"};
        BigInteger expectedSum = BigInteger.valueOf(100);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Однпоточное вычисление суммы положительных чисел");
    }

    @Test
    void calculateSumInResourcesUrl() {
        //arrange
        String[] resouces = new String[]{
                "https://gitlab" +
                        ".com/Innopolis/PolitovFirstHomeWork/raw" +
                        "/80da0d256c77086d38702db2e0ee4da68b0cf1cd/big%20data.txt"};
        BigInteger expectedSum = BigInteger.valueOf(4300);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Однпоточное вычисление суммы положительных чисел из ресурса" +
                        "URL");
    }

    @Test
    void calculateSumInResourcesManyThread() {
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
                "src\\test\\java\\tests\\resources\\positiveNumberSum100.txt",
        };
        BigInteger expectedSum = BigInteger.valueOf(700);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Многопоточное вычисление суммы положительных чисел");
    }

    @Test
    void calculateSumInResourcesNegativeNumber() {
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources\\negativeSum0.txt"};
        BigInteger expectedSum = BigInteger.valueOf(0);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Однпоточное вычисление суммы положительных чисел из ресурса" +
                        "только с отрицательными числами");
    }

    @Test
    void calculateSumInResourcesOddNumber() {
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources\\positivOddSum0.txt"};
        BigInteger expectedSum = BigInteger.valueOf(0);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Однпоточное вычисление суммы положительных чисел из ресурса" +
                        "только с положительными нечетными числами");
    }

    @Test
    void calculateSumInResourcesPositiveNegativeNumber() {
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources" +
                        "\\positiveWithNegativeSum100.txt"};
        BigInteger expectedSum = BigInteger.valueOf(100);

        //act + assert
        simpleTestCalculateSumInResources(expectedSum, resouces,
                "Однпоточное вычисление суммы положительных чисел из ресурса" +
                        "с положительными и отрицательными числами");
    }

    @Test
    void calculateSumInResourcesNotExisits() {
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources\\fileNotFound.txt"};
        BigInteger expectedSum = BigInteger.valueOf(100);

        //act + assert
    }

    @Test
    void calculateSumInResourcesIncorrectData() {

        //TODO: разобраться почему после выполнения этого теста остальные
        /*
        //   могут завалиться
        //arrange
        String[] resouces = new String[]{
                "src\\test\\java\\tests\\resources\\incorrectDataException.txt"};

        //act + assert
        assertThrows(EParser.class, new Executable() {
            @Override
            public void execute() throws Throwable {
                resourcesProcessor
                        .calculateSumInResources(resouces);
            }
        });
        */

    }

}