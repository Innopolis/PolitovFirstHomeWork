package tests;

import innopolis.politov.calculator.Calculator;
import org.junit.jupiter.api.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.LongSummaryStatistics;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Created by General on 2/13/2017.
 */
class CalculatorTest {
    Calculator calculator;
    Thread threadCalc;
    BigInteger sum;
    @BeforeEach
    void setUp() {
        sum = BigInteger.valueOf(0);
        calculator = new Calculator(newValue -> sum = newValue);
        assertNotNull(calculator);
        threadCalc = new Thread(calculator);
    }

    @AfterEach
    void tearDown() {
        calculator = null;
        threadCalc = null;
        sum = null;
    }

    @Test
    void getSum() {
        //assert inittialization
        assertEquals(BigInteger.valueOf(0),calculator.getSum());

        //arrange
        LinkedList<Long> longLinkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            longLinkedList.add(Long.valueOf(i));
        }
        calculator.getElementList().addAll(0, longLinkedList);

        //act
        threadCalc.start();
        calculator.calculateStope();
        try {
            threadCalc.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //assert
        assertEquals(BigInteger.valueOf(45), calculator.getSum(),
                "Полученная калькулятором сумма, после вычмслений, не " +
                        "совпадает с ожидаемой");
        assertEquals(sum, calculator.getSum(), "Калькулятор не обновил сумму" +
                " во внешней переменной");
    }

    @Test
    void getElementList() {
        assertTrue( calculator.getElementList().size() == 0 , "После " +
                "создания объекта-калькулятора, коллекция суммируемых " +
                "элементов не пуста");
        calculator.getElementList().add(Long.valueOf(0));
        assertTrue(calculator.getElementList().size() == 1, "Не добавляются" +
                " новые значения в коллекцию");

    }


    @Test
    void runWithBeforeDataSet() {
        //arrange
        LinkedList<Long> longLinkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            longLinkedList.add(Long.valueOf(i));
        }
        calculator.getElementList().addAll(0, longLinkedList);

        //act
        threadCalc.start();
        calculator.calculateStope();
        try {
            threadCalc.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //assert
        assertEquals(BigInteger.valueOf(45), calculator.getSum(),
                "Полученная калькулятором сумма, после " +
                        "вычислений, не " +
                        "совпадает с ожидаемой" +
                        " ");
        assertEquals(sum, calculator.getSum(), "Калькулятор не обновил сумму" +
                " во внешней переменной");

    }

    @Test
    void runWithoutDataUpdate() {
        //arrange
        calculator = new Calculator((newValue) -> { return; });
        threadCalc = new Thread(calculator);
        LinkedList<Long> longLinkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            longLinkedList.add(Long.valueOf(i));
        }
        calculator.getElementList().addAll(0, longLinkedList);

        //act
        threadCalc.start();
        calculator.calculateStope();
        try {
            threadCalc.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //assert
        assertEquals(BigInteger.valueOf(45), calculator.getSum(),
                "Полученная калькулятором сумма, после " +
                        "вычислений, не " +
                        "совпадает с ожидаемой" +
                        " ");
        assertEquals(BigInteger.valueOf(45), calculator.getSum(),
                "Калькулятор обновил сумму" +
                " во внешней переменной");

    }

    @Test
    void runWithNotDataSet() {
        //arrange

        //act
        threadCalc.start();
        calculator.calculateStope();
        try {
            threadCalc.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        //assert
        assertEquals(BigInteger.valueOf(0), calculator.getSum(),
                "Полученная калькулятором сумма, после " +
                        "вычмслений, не " +
                        "совпадает с ожидаемой" +
                        " ");
        assertEquals(sum, calculator.getSum(), "Калькулятор не обновил сумму" +
                " во внешней переменной");

    }


    @Test
    void calculateStope() {
        //arrange
        //act
        threadCalc.start();
        try {
            threadCalc.join(1000);

            //assert
            assertTrue(threadCalc.isAlive(), "calculateStope(): Поток вычислений" +
                    " не запустился");
            calculator.calculateStope();
            threadCalc.join(1000);
            //assert после остановки
            assertFalse(threadCalc.isAlive(), "calculateStope(): Поток " +
                    "вычислений не остановился");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Test
    void isEven() {

        boolean result = false;
        //act
        result =  Calculator.isEven(Long.valueOf(0));
        assertTrue(result, "0 - четное");

        //act
        result =  Calculator.isEven(Long.valueOf(1));
        assertFalse(result, "1 - нечетное");

        //act
        result =  Calculator.isEven(Long.valueOf(2));
        assertTrue(result, "2 - четное");

        //act
        result =  Calculator.isEven(Long.valueOf(-2));
        assertTrue(result, "-2 - четное");

        //act
        result =  Calculator.isEven(Long.valueOf(-1));
        assertFalse(result, "-1 - нечетное");
    }

}