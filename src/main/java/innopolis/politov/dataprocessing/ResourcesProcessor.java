package innopolis.politov.dataprocessing;

import innopolis.politov.calculator.Calculator;
import innopolis.politov.parser.EParser;
import innopolis.politov.parser.IntegerElementParser;
import innopolis.politov.threadmanager.ThreadManager;
import innopolis.politov.viewer.IDisplay;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Created by General on 2/11/2017.
 * Класс, который получает на вход список ресурсов,
 * содержащих набор чисел и считает сууму всех положительных четных.
 * Каждый ресурс обработывается в отдельном потоке, набор должен содержать лишь
 * числа, унарный оператор "-" и пробелы. Общая сумма должна отображаться на экране
 * и изменяться в режиме реального времени.
 */
public class ResourcesProcessor {

    public static final Logger logger = Logger.getLogger(ResourcesProcessor.class);
    static {
        DOMConfigurator.configure
                ("src/main/java/innopolis/politov/resources/ResourcesProcessorLogger" +
                        ".xml");
    }

    public ResourcesProcessor(IDisplay display) {
        this.display = display;
    }

    /**
     * Метод, который получает на вход список ресурсов,
     * содержащих набор чисел и считает сууму всех положительных четных.
     * Каждый ресурс обработывается в отдельном потоке, набор должен содержать лишь
     * числа, унарный оператор "-" и пробелы. Общая сумма должна отображаться на экране
     * и изменяться в режиме реального времени.
     * @param resourcePathes
     */
    public BigInteger calculateSumInResources(String[] resourcePathes){
        String loggerMessage;
        logger.info("Parser initialization is begun...");
        Calculator calculator = new Calculator( (newValue) -> {
            if(display!=null)
                display.displayMessage(newValue.toString());
        });
        Thread calcThread = new Thread(calculator, "calculatorThread");
        ArrayList<Object> parserList = new ArrayList<>(resourcePathes.length);
        try {
            calcThread.start();
            ThreadManager threadManager = new ThreadManager(resourcePathes.length);

            for (String resourcesName :
                    resourcePathes) {
                IntegerElementParser integerElementParser = null;

                try {
                    integerElementParser = new IntegerElementParser(
                            resourcesName,
                            calculator.getElementList(),
                            (long value) -> ((value > 0) && Calculator.isEven(value))
                    );
                    integerElementParser.setDisplay(display);
                    parserList.add(integerElementParser);
                } catch (IOException e) {
                    loggerMessage  = "Error accessing the resource: \"" +
                            resourcesName + "\"";
                    displayMessage(loggerMessage);
                    logger.error(loggerMessage, e);
                }
                threadManager.addThread(new Thread(integerElementParser, "Thread: " +
                        "" + resourcesName));
                logger.info("   Parser:"+ resourcesName +" - " +
                        "inetialized");
            }
            logger.info("Parser initializations are finished");
            logger.info("Parser start...");
            threadManager.startAndWaitAllThread();
        }finally {
            calculator.calculateStope();
            try {
                calcThread.join();
            } catch (InterruptedException e) {
                logger.error("Thread parser is interupted. ", e);
            }
        }
        if (checkResultForEach(parserList)) {
            logger.info("Parsers succesfull completed");
            loggerMessage = "Elemen count =  " +
                    calculator.getElementList()
                    .size() + "\nTotal sum = " + calculator.getSum();
            displayMessage(loggerMessage);
            logger.info(loggerMessage);
            return calculator.getSum();
        }
        return null;
    }

    private  IDisplay display;

    private void displayMessage(String mess){
        if(display != null){
            display.displayMessage(mess);
        }
    }

    private boolean checkResultForEach(ArrayList<Object> parserList) throws EParser {
        IntegerElementParser integerElementParser;
        boolean expectedError = false;
        for (Object parser :
                parserList) {
            integerElementParser = (IntegerElementParser)parser;
            expectedError = expectedError || integerElementParser
                    .isSelfStoped();
            if (expectedError){
                String loggerMessage = "Процесс обработки входных ресурсов " +
                        "не завершен.";
                displayMessage(loggerMessage);
                logger.info(loggerMessage);
                if (integerElementParser.getException()!=null) {
                    logger.error("Возникла ошибка при обработке ресурса",
                            integerElementParser.getException());
                }
            }
        }
        return !expectedError;


    }

}
