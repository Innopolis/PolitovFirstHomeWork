package innopolis.politov.threadmanager;

import innopolis.politov.dataprocessing.ResourcesProcessor;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.util.ArrayList;

/**
 * Created by General on 2/11/2017.
 * Диспетчер потоков
 */
public class ThreadManager {
    public static final Logger logger = Logger.getLogger(ResourcesProcessor.class);
    static {
        DOMConfigurator.configure
                ("src/main/java/innopolis/politov/resources/ResourcesProcessorLogger" +
                        ".xml");
    }

    /**
     * Конструктор диспетчера потоков, задающий начальную ёмкость диспетчера и
     * флаг ожидания завершения работы всех потоков в диспетчере при их
     * запуске черех функцию stat (кроме startAndWaitAllThread)
     * @param beginingThreadCount
     */
    public ThreadManager(Integer beginingThreadCount) {
        threads =new ArrayList<>(beginingThreadCount);
    }


    /**
     * Добавляет поток в список дисптчеризуемых потоков
     * @param thread - добавляемый поток
     */
    public void addThread(Thread thread){
        threads.add(thread);
        logger.debug("New thread added in to dispetcher: \"" + thread
                .getName() + "\"");
    }

    /**
     * Запускает все потоки, находящиеся в списке диспетчера, и дожидается их
     * завершения
     */
    public void startAndWaitAllThread(){
        try {
            startAllThread();
            joinedAllThread();
        }
        finally{
            joinedAllThread();
        }
    }

    /**
     * Запускает все потоки, находящиеся в списке диспетчера
     */
    public void startAllThread(){
        logger.debug("All thread is starting...");
        for (Thread thread :
                threads) {
            if (!thread.isAlive()){
                thread.start();
            }
        }
    }
    /**
     * Ожидание завершения работы потоков находящихся в списке диспетчера
     */
    public void joinedAllThread(){
        logger.debug("Wait finishing all thread...");
        for (Thread thread :
                threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private ArrayList<Thread> threads;
}
