package innopolis.politov.viewer;

/**
 * Created by General on 2/11/2017.
 */
public interface IValueNotify<T> {
    void updateValue(T newValue);
}
