package innopolis.politov.viewer;

/**
 * Created by General on 2/11/2017.
 * Функциональный интерфейс для отображения сообщений
 */
public interface IDisplay {
    void displayMessage(String message);
}
