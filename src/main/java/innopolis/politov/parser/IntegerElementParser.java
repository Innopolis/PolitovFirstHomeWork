package innopolis.politov.parser;

import innopolis.politov.dataprocessing.ResourcesProcessor;
import innopolis.politov.viewer.IDisplay;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.*;
import java.util.Collection;
import java.util.LinkedList;
import java.util.Scanner;


/**
 * Created by General on 2/7/2017.
 * Парсер входных ресурсов и добавление их распарсенных целочисленных значений,
 * отвечающих заданному условию, в коллекцию
 */
public class IntegerElementParser implements Runnable {
    public static final Logger logger = Logger.getLogger(ResourcesProcessor.class);
    static {
        DOMConfigurator.configure
                ("src/main/java/innopolis/politov/resources" +
                        "/ResourcesProcessorLogger" +
                        ".xml");
    }

    /*private field*/
    private  InputStream is;
    private  LinkedList<Long> collection;
    private IConditional IConditional;
    private String resourcePath;
    private String  globalErrorMessage;
    private String selfErrorMessage;
    private EParser exception;


    public IntegerElementParser(String resourcePath, LinkedList<Long>
            collection, innopolis.politov.parser.IConditional IConditional) throws IOException {
        this.resourcePath = resourcePath;
        this.is = CreateAbstractInputStream.createInputStream(resourcePath);
        this.collection = collection;
        this.IConditional = IConditional;
    }

    /**
     * Флага останова процесса парсинга ресурса(процесс не был завершен) для
     * всех потоков данного класса
     * @return true - флаг установлен, false - снят
     */
    public static boolean isStopedAll() {
        return stopedAll;
    }

    /**
     * Флага останова процесса парсинга ресурса(процесс не был завершен) для
     * всех потоков данного класса. Флаг необходимо установить до начала
     * процесса обработки
     * @param stopedAll true - установить флаг, false - снять флаг
     */
    public static void setStopedAll(boolean stopedAll) {
        logger.debug("Принудительная остановка всех потоков-обработчиков...");
        setAllStoped = stopedAll;
    }

    /**
     *
     * @return
     */
    public EParser getException() {
        return exception;
    }

    /**
     * интерфейс отображения процесса обработки ресурса
     * @param display интерфейс отображения процесса обработки ресурса
     */
    public void setDisplay(IDisplay display) {
        this.display = display;
    }

    /**
     * Опрос флага останова процесса парсинга ресурса(процесс не был завершен)
     * @return true - процесс парсинга ресурса был остановлен(процесс не был
     * завершен)
     */
    public boolean isSelfStoped() {
        return selfStoped;
    }

    /**
     * Флаг останова процесса парсинга ресурса(процесс не был завершен)
     * @param selfStoped true - приводит к отложенному (до окончания итерации
     *                   очередной парсинга ресурса) останову потока
     *                   Значение false - снимает флаг (не рекомендуется к
     *                   использованию)
     */
    public void setSelfStoped(boolean selfStoped) {
        this.selfStoped = selfStoped;
    }

    /**
     *  * Парсер входных ресурсов и добавление их распарсенных целочисленных значений,
     * отвечающих заданному условию, в коллекцию
     */
    @Override
    public void run() {
        long tempLong = 0;
        try(Reader reader  = new InputStreamReader(is, "UTF-8" );

            Scanner scanner = new Scanner(reader)){
            while ((!stopedAll) && (!selfStoped)) {
                if(scanner.hasNextLong()) {
                    tempLong = scanner.nextLong();
                    if (IConditional.IsConditional(tempLong))
                        synchronized (collection) {
                            collection.add(Long.valueOf(tempLong));
                        }
                }
                else if (scanner.hasNext()){
                    setException("Ошибка формата ресурса: обнаружены " +
                            "некорректные данные в входном ресурсе\"" + resourcePath
                            + "\"", new Exception());
                    break;
                }else return;
            }

        } catch (UnsupportedEncodingException e) {
            setException("Ошибка формата ресурса: обнаружены " +
                    "некорректные данные в входном ресурсе\"" + resourcePath
                    + "\"", e);
        } catch (IOException e) {
            setException("Возникла, ошибка ввода-вывода при " +
                    "работе с ресурсом: \"" + resourcePath + "\"", e);
        }
        finally {
                if (display != null)
                    display.displayMessage("Поток завершил работу: " + Thread
                            .currentThread().getName());

        }

    }


    /**
     * Требуется ли остановить все потоки данного типа, при возникновении
     * ошибки (программной остановки), хотя бы в одном
     */
    private static volatile  boolean setAllStoped = true;
    private static  volatile boolean stopedAll = false;
    private  volatile boolean selfStoped = false;
    private  IDisplay display;

    /*private method*/
    private void setException(String message, Exception e){
        exception = new EParser(message, e);
        selfErrorMessage = message;
        logger.error("Ошибка обработки ресурсов: " + message, e);
        if(setAllStoped){
            setStopedAll(true);
            setSelfStoped(true);
        }
        else {
            setSelfStoped(true);
        }
        if(display != null)
            display.displayMessage(message);
    }

}



