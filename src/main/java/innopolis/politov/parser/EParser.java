package innopolis.politov.parser;

/**
 * Created by General on 2/11/2017.
 * Общее исключение для всех ошибок парсера
 */
public class EParser extends RuntimeException {
    public EParser(String message, Throwable cause) {
        super(message, cause);
    }
}
