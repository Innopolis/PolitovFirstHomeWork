package innopolis.politov.parser;

/**
 * Функциональный интерфейс проверки удовлетворения состоянию
 * Created by General on 2/8/2017.
 */
public interface IConditional {
    boolean IsConditional(long value);
}
