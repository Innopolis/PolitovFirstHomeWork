package innopolis.politov;


import innopolis.politov.dataprocessing.ResourcesProcessor;

public class Main {

    public static void main(String[] args) {
        ResourcesProcessor resourcesProcessor = new ResourcesProcessor(
                null);
        resourcesProcessor.calculateSumInResources(args);
    }
}
