package innopolis.politov.calculator;

import innopolis.politov.dataprocessing.ResourcesProcessor;
import innopolis.politov.viewer.IValueNotify;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import java.math.BigInteger;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Created by General on 2/7/2017.
 */
public class Calculator implements Runnable{

    public static final Logger logger = Logger.getLogger(Calculator.class);
    static {
        DOMConfigurator.configure
                ("src/main/java/innopolis/politov/resources/ResourcesProcessorLogger" +
                        ".xml");
    }

    private int currentElementIndex = 0;
    private volatile   BigInteger sum = BigInteger.valueOf(0);
    private LinkedList<Long> listElement = new LinkedList<>();
    private volatile boolean calculateStoped = false;
    private IValueNotify<BigInteger> notifier;

    /**
     * конструктор, задающий итерфейс для вывода результатов вычисления в
     * реальном времени
     * @param notifier итерфейс для вывода результатов вычисления в реальном времени
     */
    public Calculator(IValueNotify<BigInteger> notifier) {
        this.notifier = notifier;
    }

    /**
     * Получение текущего значения суммы коллекции элементов getElementList()
     * @return текущее значение суммы коллекции элементов getElementList()
     */
    public BigInteger getSum() {
        return sum;
    }

    /**
     * коллекция целочисленных элементов
     * @return коллекция целочисленных элементов
     */
    public LinkedList<Long> getElementList() {
        return listElement;
    }

    /**
     *вычисляетс суммму элементов коллекции getElementList()
     * обновленное значение после каждого сумирования передает через интерфейс IValueNotify<BigInteger>
     * значения в коллекцию можно добавлять во время вычислений
     * getElementList().add(...)
     * вычисление суммы продолжается до останова, путем вызова функции calculateStope
     */
    @Override
    public void run() {
        logger.info("Calculating sum is begin");
        int index = 0;
        ListIterator<Long> iterator ;
        while (true){
            boolean isLast  = calculateStoped;
            synchronized (listElement) {
                iterator = listElement.listIterator(index);
                while (iterator.hasNext()) {
                    sum = sum.add(BigInteger.valueOf(iterator.next()));
                    if (notifier != null) {
                        notifier.updateValue(sum);
                    }
                    logger.info("[ Sum: " + sum.toString() + "]");
                }
            }
            index = iterator.nextIndex();
            index = index < 0 ? iterator.previousIndex() : index;
            if (isLast) {
                logger.info("Calculating sum is finished");
                return;
            }
        }
    }

    /**
     * Остановить вычисление, дождавшись завершения очередной итерации
     */
    public void calculateStope(){
        calculateStoped = true;
    }

    /**
     * Определение четности числа
     * @param number число
     * @return true - если число четное
     *          false - нечетное
     */
    public static boolean isEven(Long number){
        return ( (number & 1) == 0 );
    }

}
